import { configureStore } from "@reduxjs/toolkit";
import RestaurantReducer from "./reducer";

export default configureStore({
	reducer: {
		restaurant: RestaurantReducer,
	},
});
