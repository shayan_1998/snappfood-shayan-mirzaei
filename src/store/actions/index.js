import { createAsyncThunk } from "@reduxjs/toolkit";
import Api from "../../api";

// create a async action
// use Api class to fetch data from server
const getRestaurantList = createAsyncThunk(
	"restaurant/getList",
	async (arg, { getState }) => {
		const state = getState();
		return await Api.getInstance()
			.restaurantService()
			.getVendorList(state.restaurant.page);
	}
);

export default getRestaurantList;
