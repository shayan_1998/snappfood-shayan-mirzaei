import { createSlice } from "@reduxjs/toolkit";
import getRestaurantList from "../actions";

export const restaurantSlice = createSlice({
	name: "restaurant",
	initialState: {
		page: 0,
		list: [],
		loading: false,
	},
	//handle different state of async action
	extraReducers: {
		[getRestaurantList.pending]: (state) => {
			state.loading = true;
		},
		[getRestaurantList.fulfilled]: (state, action) => {
			state.list = [...state.list, ...action.payload];
			state.page++;
			state.loading = false;
		},
		[getRestaurantList.rejected]: (state) => {
			state.page--;
			state.loading = false;
		},
	},
});

export default restaurantSlice.reducer;
