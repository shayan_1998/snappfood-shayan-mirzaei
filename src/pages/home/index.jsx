import React from "react";
import { Link } from "react-router-dom";
import "./index.scss";

const Home = () => {
	return (
		<div className="homepage">
			<h1>Welcome</h1>
			<div className="homepage__info">
				Please click on blow link to navigate to restaurants page
			</div>
			<Link to="/restaurant">Restaurant</Link>
		</div>
	);
};

export default Home;
