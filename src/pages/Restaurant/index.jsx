import React, { useEffect, useRef } from "react";
import "./index.scss";
import RestaurantCard from "../../components/card/RestaurantCard";
import { useDispatch, useSelector } from "react-redux";
import getRestaurantList from "./../../store/actions";
import {
	List,
	AutoSizer,
	CellMeasurer,
	CellMeasurerCache,
} from "react-virtualized";

const INFINITY_LOADER_THRESHOLD = 1000;

const Restaurant = () => {
	// calculate item size and cache them for react-virtualized
	const rowItemCache = useRef(
		new CellMeasurerCache({
			fixedWidth: true,
			defaultHeight: 250,
		})
	);
	//res data from redux store
	const restaurantList = useSelector((state) => state.restaurant.list);
	const loading = useSelector((state) => state.restaurant.loading);
	const dispatch = useDispatch(); // prepare redux dispatch

	useEffect(() => {
		fetch();
	}, [dispatch]);

	const fetch = () => {
		dispatch(getRestaurantList()); // dispatch async action of fetching data
	};

	// handle load more on list scroll
	const onScroll = ({
		clientHeight = 0,
		scrollHeight = 0,
		scrollTop = 0,
	}) => {
		if (clientHeight === 0) return;
		if (scrollHeight - INFINITY_LOADER_THRESHOLD < scrollTop && !loading) {
			fetch();
		}
	};

	return (
		<div className="restaurants">
			{loading && restaurantList.length == 0 ? (
				<div>درحال دریافت اطلاعات...</div>
			) : (
				<AutoSizer>
					{({ width, height }) => (
						<List
							onScroll={onScroll}
							width={width}
							height={height}
							rowHeight={rowItemCache.current.rowHeight}
							deferredMeasurementCache={rowItemCache.current}
							rowCount={restaurantList.length}
							rowRenderer={({ key, index, style, parent }) => (
								<CellMeasurer
									key={key}
									cache={rowItemCache.current}
									parent={parent}
									columnIndex={0}
									rowIndex={index}
								>
									<div
										style={style}
										className="virtual-list-item"
									>
										<RestaurantCard
											data={restaurantList[index]}
										/>
									</div>
								</CellMeasurer>
							)}
						/>
					)}
				</AutoSizer>
			)}
		</div>
	);
};

export default Restaurant;
