export const delimiter = (num) => {
	return num
		.toString()
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
