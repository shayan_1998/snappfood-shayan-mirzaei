import React from "react";
import PropTypes from "prop-types";
import "./index.scss";
import { delimiter } from "../../utils/delimiter/index";
import Rate from "../rate";

const RestaurantCard = ({ data }) => {
	return (
		<div className="card">
			<div className="card__cover">
				{!!data.has_coupon && (
					<div className="card__best-coupon medium">
						{data.best_coupon}{" "}
						{data.coupon_count > 1 &&
							`و ${data.coupon_count - 1} پیشنهاد دیگر`}
					</div>
				)}
				<img src={data.backgroundImage} alt={data.title} />
				<div className="card__logo">
					<img src={data.logo} alt={data.title} />
				</div>
			</div>
			<div className="card__info">
				<div className="card__head">
					<div className="card__name">
						<span className="bold card__name--title">
							{data.title}
						</span>
						{!!data.discountValueForView && (
							<span className="card__discount-badge">
								تا{data.discountValueForView}%
							</span>
						)}
					</div>

					{data.rate ? (
						<>
							<div className="card__view">({data.voteCount})</div>
							<Rate number={data.rate} />
						</>
					) : (
						<div className="card__new">جدید</div>
					)}
				</div>
				<div className="card__description">
					{data.description.replace(/,/g, "")}
				</div>
				<div className="card__deliveryFee">
					<span>
						{data.isZFExpress ? "ارسال اکسپرس" : "پیک فروشنده"}
					</span>
					{data.deliveryFee
						? `${delimiter(data.deliveryFee)} تومان`
						: "رایگان"}
				</div>
			</div>
		</div>
	);
};

export default RestaurantCard;

RestaurantCard.propTypes = {
	data: PropTypes.object,
};
