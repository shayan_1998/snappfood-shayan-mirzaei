import React from "react";
import PropTypes from "prop-types";
import "./index.scss";

const Rate = ({ number }) => {
	return (
		<div
			className={`card__rate 
                ${number < 2 && "card__rate--low"}
                ${number >= 2 && number < 3 && "card__rate--mid"}
                ${number >= 3 && number < 4.5 && "card__rate--good"}
                ${number >= 4.5 && "card__rate--excellent"}
            `}
		>
			<span className="icon-star" /> {number.toFixed(1)}
		</div>
	);
};

export default Rate;

Rate.propTypes = {
	number: PropTypes.number,
};
