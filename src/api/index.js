import axios from "axios";
import Restaurant from "./services/Restaurant";

export default class Api {
	instance = null; // singleton instance
	axios; // axios instance
	services = new Map(); // a map of our services

	constructor() {
		this.setupAxios();
		this.setupServices();
	}

	//initiate axios object and
	setupAxios() {
		// initialize axios instance
		this.axios = axios.create({
			// here we can config our instance
			baseURL: process.env.REACT_APP_BASE_URL,
		});
		// interceptor for each request
		// you can uncomment the logs statements to see log on each request
		this.axios.interceptors.request.use(
			async function (config) {
				// console.log(config.url, config);
				return config;
			},
			function (error) {
				// console.log('error: ', error);
				return Promise.reject(error);
			}
		);

		// interceptor for each response
		// you can uncomment the logs statements to see log on each response
		this.axios.interceptors.response.use(
			function (response) {
				//2**
				// console.log(
				// 	`%c${response.config.url} => ${response.status}`,
				// 	"color:#0f7404; background:#0f7404",
				// 	response
				// );
				return response;
			},
			async function (error) {
				//4** OR 5**
				// console.error(
				//     `${error.config.url} => ${error.response.status}`,
				//     error.response,
				// );

				// sample
				// we can detect error status and react to them
				// if (error.response.status == 401) {
				// logout the user
				// }

				return Promise.reject(error);
			}
		);
	}

	//subscribe services to api services map
	setupServices() {
		this.services.set("Restaurant", new Restaurant(this.axios));
	}

	//get an singleton instance of api class
	static getInstance() {
		if (this.instance != null) {
			return this.instance;
		} else {
			this.instance = new Api();
			return this.instance;
		}
	}

	//get access to each service
	restaurantService() {
		return this.services.get("Restaurant");
	}
}
