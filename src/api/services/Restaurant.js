export default class Restaurant {
	axios;

	constructor(axios) {
		//each service receive axios instance and then use it to call http requests
		this.axios = axios;
	}

	//get function => list of vendors
	async getVendorList(page) {
		return await this.axios
			.get(`restaurant/vendors-list`, {
				params: {
					page,
					page_size: 10,
					lat: 35.754,
					long: 51.328,
				},
			})
			.then((res) => {
				//distract response => we only need finalResult for new
				let { finalResult = [] } = res.data.data;
				let response = [];
				//loop trough finalResult to extract vendors data
				response = finalResult
					.filter((item) => item.type === "VENDOR")
					.map((item) => item.data);

				return response;
			})
			.catch((err) => {
				throw new Error(err.response.data.message);
			});
	}
}
