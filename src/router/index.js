import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Home from "../pages/home";
import Restaurant from "../pages/Restaurant";

const Router = () => {
	return (
		<Routes>
			<Route path="/" element={<Home />} />
			<Route path="/restaurant" element={<Restaurant />} />
			<Route path="*" element={<Navigate to="/" replace />} />
		</Routes>
	);
};

export default Router;
