# SnappFood Interview

author : shayan mirzaei

In this doc I try to explain codebase

# Project structure

## api

in this folder I manage our XHR Requests

here `index.js` file is responsible for creating a singleton instance of Api class that implement axios and necessary services

in `services` folder, create a class for every service that contains its functions (each class receive the axios instance that generated in Api class)

finally through Api instance I can access our request like :

```
<!-- its return a promise -->
Api.getInstance().restaurantService().getVendorList()
```

## components

our components implemented here.for new I only have 2 component

-   card : restaurant card component
-   rate : rating of each restaurant

(every folder at least have a js and scss file)

## pages

our pages implemented here.for new I only have 2 page

-   home : fake home page
-   restaurant : implementation of snappfood restaurant page

## router

in this folder I handle app routing with `react-router-dom` library

## store

here using `redux / reduxt-toolkit` I implemented app global state managemen

-   index.js file : create redux instance and configure it
-   reducer : handle each reducer (here I store our restaurant list and request page)
-   actions : I use `redux-thunk` to handle promise in redux

## styles

I use this folder to store our fonts / icons / global styles and etc

-   fonts : I use `iranSans` font
-   icons : I use `icomoon` to implement app icon's
-   app.scss : here I put global styles
-   color.module.scss : here I put scss color variables

## utils

the necessary utils goes here,for example I create delimiter function that get an number and delimiter it

```
delimiter(20000) // output : 20,000
```

## app.js

here I put `BrowserRouter` and use my router component

## index.js

it's starting point of our app and contain out store Provider

## .env and .env.example

our environment variables set in this file.for new I have only onw variables for our api base url

(if you receive this project through git please turn `.env.example` file to `.env`)

## `for more specific information I use comments in code`

# Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
